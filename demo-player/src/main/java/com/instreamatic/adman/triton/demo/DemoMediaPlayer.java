package com.instreamatic.adman.triton.demo;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;

public class DemoMediaPlayer {

    protected Context context;

    MediaPlayer ambientMediaPlayer;
    int streamType = AudioManager.STREAM_MUSIC;
    //****************************
    /** Service (MediaPlayer) states  */
    public enum State {
        NONE,
        PLAING,
        PAUSE,
        STOP;
    }
    State state = State.NONE;

    DemoMediaPlayer(Context context) {
        this.context = context;
    }
    //***********
    /**
     *
     * Working with MediaPlayer
     *
     **/
    private void init() {
        ambientMediaPlayer = MediaPlayer.create(this.context, R.raw.mu_weightless);
        ambientMediaPlayer.setAudioStreamType(streamType);
        //Looping the audio
        ambientMediaPlayer.setLooping(true);

        float volume = 1.0f;
        ambientMediaPlayer.setVolume(volume, volume);
    }

    public void done() {
        if(ambientMediaPlayer != null){
            ambientMediaPlayer.stop();
            ambientMediaPlayer.release();
            ambientMediaPlayer = null;
        }
    }

    public void start(){
        if(ambientMediaPlayer == null){
            init();
        }
        ambientMediaPlayer.start();
        this.state = State.PLAING;
    }

    public void pause(){
        if(ambientMediaPlayer != null && ambientMediaPlayer.isPlaying()){
            ambientMediaPlayer.pause();
        }
        this.state = State.PAUSE;
    }

    public void stop(){
        done();
        this.state = State.STOP;
    }

    public boolean isPlaying(){
        return (ambientMediaPlayer != null) && ambientMediaPlayer.isPlaying();
    }

    public void setVolume(float volume) {
        ambientMediaPlayer.setVolume(volume, volume);
    }

    public State getState() {
        return this.state;
    }
}
