package com.instreamatic.adman.triton.demo;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.RemoteViews;

import java.util.Timer;
import java.util.TimerTask;


public class DemoNotification {

    final public Notification notification;
    final public int id;

    private Context context;
    private boolean displayed;
    private Timer timer;
    private TimerTask responseTask;

    public DemoNotification(Context context, int id) {
        this.context = context;
        this.id = id;
        timer = new Timer();
        displayed = false;
        notification = build();
        notification.contentView.setOnClickPendingIntent(R.id.play, buildIntent(MediaService.START));
        notification.contentView.setOnClickPendingIntent(R.id.pause, buildIntent(MediaService.PAUSE));
        notification.contentView.setOnClickPendingIntent(R.id.cancel, buildIntent(MediaService.STOP));
    }

    protected PendingIntent buildIntent(String command) {
        Intent intent = new Intent();
        intent.setAction(command);
        intent.setClass(context, MediaService.class);
        return PendingIntent.getService(context, 0, intent, (Build.VERSION.SDK_INT >= 23 ? PendingIntent.FLAG_IMMUTABLE : 0));
    }

    protected Notification build() {
        RemoteViews contentView = new RemoteViews(context.getPackageName(), R.layout.notififcation);

        Intent intent = new Intent();
        intent.setClass(context, MediaService.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, (Build.VERSION.SDK_INT >= 23 ? PendingIntent.FLAG_IMMUTABLE : 0));


        Notification.Builder builder = new Notification.Builder(context)
                .setContent(contentView)
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_launcher);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(
                    "instreamatic",
                    context.getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_NONE);
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);
            builder.setChannelId(channel.getId());
        }
        return builder.build();
    }

    public void show() {
        displayed = true;
        redraw();
    }

    public void hide() {
        displayed = false;
        ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(id);
    }

    protected void redraw() {
        if (displayed) {
            ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE)).notify(id, notification);
        }
    }


    public void setDemoState(String state) {
        switch (state) {
            case MediaService.STOP:
                hide();
                break;
            case MediaService.START:
                notification.contentView.setViewVisibility(R.id.play, View.GONE);
                notification.contentView.setViewVisibility(R.id.pause, View.VISIBLE);
                show();
                break;
            case MediaService.PAUSE:
                notification.contentView.setViewVisibility(R.id.play, View.VISIBLE);
                notification.contentView.setViewVisibility(R.id.pause, View.GONE);
                show();
                break;
        }
    }
}
