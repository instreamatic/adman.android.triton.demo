package com.instreamatic.adman.triton.demo;


import android.Manifest;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.module.triton.DefaultAdsView;
import com.instreamatic.adman.view.generic.DefaultAdmanView;

import java.util.Date;
import java.util.TimerTask;

public class MainActivity extends Activity implements MediaService.Listener {
    private String TAG = "MainActivity";

    TimerTask tTask;

    /** Player service */
    private MediaService service;
    /** Text Field link */
    private TextView tvMessages;
    private TextInputEditText etURLAdService;
    /** Ad View (admanView) */
    DefaultAdmanView instreamaticAdsView = null;

    /** bound – state which is used to show if there is a connection with DemoAdmanPlayerService or not */
    private boolean bound;
    private IAdman adman;
    private DefaultAdsView tritonAdsView;



    /** ServiceConnection object. Used in order to establish connection with Service  */
    private ServiceConnection connection = new ServiceConnection() {
        //used to determine the time of connection and/or disconnect
        @Override
        public void onServiceConnected(ComponentName className, IBinder binder) {
            bindModule(binder);
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {
            unbindModule();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Text Field link
        tvMessages = (TextView) findViewById(R.id.tvEvents);
        tvMessages.setMovementMethod(new ScrollingMovementMethod());
        etURLAdService = (TextInputEditText) findViewById(R.id.etURLAdService);
        //mic permission
        checkPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        addMessage("onResume", true);

        //flag BIND_AUTO_CREATE, for cases when Service isn't working properly.
        Intent intent = new Intent(getApplicationContext(), MediaService.class);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    /**
     *
     * onStop(), close the Window. Disconnect from Service.
     *
     **/
    @Override
    protected void onStop() {
        super.onStop();
        addMessage("onStop", true);

        unbindModule();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    //-----------------------------------------------------------
    /**
     * Service link received. Starting communicating with Service
     **/
    private void bindModule(IBinder binder) {
        addMessage("bindModule", true);
        //received an object, that should be sent to onBind method in DemoAdmanPlayerService
        this.service = ((MediaService.B) binder).getService();
        if (service != null) {
            adman = service.getAdman();

            //listening for Service messages
            service.setListener(MainActivity.this);

            if (tritonAdsView == null) {
                tritonAdsView = new DefaultAdsView(this);
            }
            if(instreamaticAdsView == null) {
                instreamaticAdsView = new DefaultAdmanView(this);
            }
            adman.bindModule(tritonAdsView);
            adman.bindModule(instreamaticAdsView);

            if (adman.isPlaying()){
                addMessage("Show banner", true);
                tritonAdsView.show();
                instreamaticAdsView.show();
            }
        }
    }

    /**
     * Disconnected from Service.
     **/
    private void unbindModule() {
        addMessage("unbindModule", true);

        if ( tritonAdsView != null) {
            adman.unbindModule(tritonAdsView);
            tritonAdsView = null;
        }
        if (instreamaticAdsView != null) {
            adman.unbindModule(instreamaticAdsView);
            instreamaticAdsView = null;
        }
        adman = null;

        if (service != null) {
            service.setListener(null);

            unbindService(connection);
            service = null;
        }
        bound = false;
    }

    /**
     * Button clicks
     **/
    public void onClickButton(View v) {
        int idView = v.getId();
        switch (idView) {
            case R.id.btnStart:
                this.sendCommand(MediaService.START);
                break;
            case R.id.btnStop:
                this.sendCommand(MediaService.STOP);
                break;
            case R.id.rbAuto:
                this.sendCommand(MediaService.AD_TYPE_AUTO);
                break;
            case R.id.rbAdman:
                this.sendCommand(MediaService.AD_TYPE_ADMAN);
                break;
            case R.id.rbTriton:
                this.sendCommand(MediaService.AD_TYPE_TRITON);
                break;
            case R.id.rbTritonVideo:
                this.sendCommand(MediaService.AD_TYPE_TRITON_VIDEO);
                break;
        }
    }

    /**
     * Command sent to Service
     **/
    private void sendCommand(String action) {
        //doneAdman();
        Intent serviceIntent = new Intent();

        serviceIntent.putExtra(MediaService.AD_URL, etURLAdService.getText().toString());

        serviceIntent.setAction(action);
        serviceIntent.setClass(getApplicationContext(), MediaService.class);
        startService(serviceIntent);
    }


    /**
     * Message logging enabled
     **/
    private void addMessage(String msg, boolean toDisplay) {
        Log.d(TAG, msg);
        final String str = msg;
        if(!toDisplay) {
            return;
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Date now = new Date();
                String date = String.format("%1$tT.%1$tL", now);
                tvMessages.setText(String.format("%s %s \n", date, str)+tvMessages.getText());
            }
        });
    }

    //MediaService.Listener
    @Override
    public void onMessage(String msg) {
        this.addMessage(msg, true);
    }

    final private static int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    /**
     * Mic access
     **/
    private void checkPermission() {
        View mLayout = (View) findViewById(R.id.main_layout);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                try {
                    Snackbar.make(mLayout, R.string.permission_record_audio_rationale,
                            Snackbar.LENGTH_INDEFINITE)
                            .setAction(R.string.ok, new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    ActivityCompat.requestPermissions(MainActivity.this,
                                            new String[]{Manifest.permission.RECORD_AUDIO},
                                            PERMISSIONS_REQUEST_RECORD_AUDIO);
                                }
                            })
                            .show();
                } catch (Exception ex) {

                }
            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        PERMISSIONS_REQUEST_RECORD_AUDIO);

                // PERMISSIONS_REQUEST_RECORD_AUDIO is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_RECORD_AUDIO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
}