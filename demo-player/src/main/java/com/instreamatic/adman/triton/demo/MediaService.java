package com.instreamatic.adman.triton.demo;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.instreamatic.adman.Adman;
import com.instreamatic.adman.AdmanRequest;
import com.instreamatic.adman.IAdman;
import com.instreamatic.adman.Region;
import com.instreamatic.adman.Type;
import com.instreamatic.adman.event.AdmanEvent;
import com.instreamatic.adman.module.triton.TritonModule;
import com.instreamatic.adman.voice.AdmanVoice;
import com.instreamatic.embedded.core.LanguageCode;
import com.instreamatic.embedded.recognition.VoiceRecognitionEmbedded;
import com.tritondigital.ads.AdRequestBuilder;

import java.util.Timer;
import java.util.TimerTask;

public class MediaService extends Service implements AdmanEvent.Listener, AudioManager.OnAudioFocusChangeListener {
    private String TAG = "MediaService";

    /** commands to control the Service - onStartCommand*/
    final public static String START = "start";
    final public static String PAUSE = "pause";
    final public static String STOP = "stop";
    final public static String RESUME = "resume";
    final public static String AD_TYPE_AUTO = "ad_type_auto";
    final public static String AD_TYPE_TRITON_VIDEO = "ad_type_triton_video";
    final public static String AD_TYPE_TRITON = "ad_type_triton";
    final public static String AD_TYPE_ADMAN = "ad_type_adman";
    final public static String AD_URL = "ad_url_string";

    private String adType = AD_TYPE_AUTO;
    private String state;

    /** MediaPlayer */
    DemoMediaPlayer demoPlayer;
    private DemoNotification notification;
    //###########
    /** Timer */
    Handler tHandler;
    Timer timer;
    TimerTask tTask;
    long interval = 30 * 1000;
    //###########

    private IAdman adman;
    private boolean stateAdLoaded = false;
    private int stateAudioFocus = 0;
    private String adURLService = null;

    /** adding the new method to Binder, the method name is getService, returns the DemoAdmanPlayerService. */
    public class B extends Binder {
        public MediaService getService() {
            return MediaService.this;
        }
    }
    final protected B binder = new B();

    /** message interface */
    public interface Listener {
        void onMessage(String msg);
    }
    private Listener listener;
    //----------------------------


    public MediaService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind");
        //using this, client can call public methods in MediaService
        return binder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG,"onCreate");
        //MediaPlayer - media content of the app
        demoPlayer = new DemoMediaPlayer(this);

        //Creating Adman-object to handle ads
        initAdman();

        //Planner which is used to run ads by intervals
        initScheduler();

        notification = new DemoNotification(getApplicationContext(), 1);
        startForeground(notification.id, notification.notification);

        //Return Audio Focus
        abandonAudioFocus();
    }

    /**
     *
     * Called when Service stopped working
     *
     **/
    @Override
    public void onDestroy() {
        stopForeground(true);
        notification = null;

        doneAdman();

        //Media Player stopped
        if(demoPlayer != null) {
            demoPlayer.done();
            demoPlayer = null;
        }
        setListener(null);
    }

    public IAdman getAdman() {
        return adman;
    }

    /**
     *
     * Called when command received by Service
     *
     **/
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final String stringAction = intent.getAction();
        adURLService = intent.getStringExtra(AD_URL);
        switch (stringAction) {
            case START:
                start();
                break;
            case PAUSE:
                pause();
                break;
            case STOP:
                stop();
                break;
            case RESUME:
                resume();
                break;
            case AD_TYPE_AUTO:
            case AD_TYPE_ADMAN:
            case AD_TYPE_TRITON_VIDEO:
            case AD_TYPE_TRITON:
                adType = stringAction;
                this.updateAdRequest();
                break;
        }
        return START_STICKY;
    }

    private void changeDemoState(String state) {
        if (this.state != state) {
            this.state = state;
            notification.setDemoState(state);
        }
    }

    private void start() {
        Log.d(TAG,"CMD start");
        changeDemoState(START);

        requestAudioFocus();
        resume();
    }

    private void stop() {
        Log.d(TAG,"CMD stop");
        changeDemoState(STOP);

        stopScheduler();
        stopAdman();
        demoPlayer.stop();
        abandonAudioFocus();
    }

    private void pause() {
        Log.d(TAG,"CMD pause");
        changeDemoState(PAUSE);

        stopScheduler();
        demoPlayer.pause();
        pauseAdman();
    }

    private void resume() {
        Log.d(TAG,"resume");
        changeDemoState(RESUME);

        //if the last state of the player was ‘PLAYING’ but there is no actual playback, start the player
        if (adman.isPlaying()) {
            resumeAdman();
        } else {
            Log.d(TAG,"demoPlayer.start");
            demoPlayer.start();
        }
        startScheduler();
    }

    private void initAdman() {
        AdmanRequest.Builder builder = getAdRequest();
        adman = new Adman(this, builder.build());

        //for recognition on the device
        VoiceRecognitionEmbedded voiceRecognitionEmbedded = new VoiceRecognitionEmbedded(this);
        voiceRecognitionEmbedded.addModel("assets/models/large_instrmtc_ovr25_enUS.snsr", LanguageCode.ENGLISH_US);

        AdmanVoice admanVoice = new AdmanVoice(this);
        admanVoice.setVoiceRecognition(voiceRecognitionEmbedded);
        adman.bindModule(admanVoice);

        adman.bindModule(new TritonModule());

        Bundle extra = new Bundle();
        extra.putBoolean("adman.need_audio_focus", false);
        adman.setParameters(extra);

        adman.addListener(this);

    }

    private void doneAdman() {
        if (adman != null) {
            adman.removeListener(this);
            adman.reset();
            adman = null;
        }
    }

    private void resumeAdman() {
        adman.play();
    }

    private void pauseAdman() {
        adman.pause();
    }

    private void stopAdman() {
        //false - Ad should be preloaded
        stateAdLoaded = false;
        //skip the current ad
        adman.skip();
    }

    private void preloadAdman() {
        adman.preload();
    }

    private void startAdman() {
        //!!! Important!!! Report the Ad Placement
        adman.sendCanShow();
        //Run an Ad
        adman.play();
    }

    /**
     *
     * listener for Ad object events, AdmanEvent.Listener
     *
     **/
    @Override
    public void onAdmanEvent(final AdmanEvent event) {
        addMessage("onAdmanEvent: " + event.getType().name());

        switch (event.getType()) {
            case READY:
                //true - ad preloaded
                stateAdLoaded = true;
                break;
            case NONE:
            case FAILED:
            case COMPLETED:
                //ad playback stopped
                Log.d(TAG,"onAdmanEvent: demoPlayer.start");
                demoPlayer.start();
                adman.reset();
                stateAdLoaded = false;
                break;
            case STARTED:
                //ad playback started
                //false - means ad should be preloaded
                demoPlayer.pause();
                stateAdLoaded = false;
                break;
        }
    }

    //###########
    /**
     *
     * Scheduler and timer for ad playback
     * actions depends on stateAdLoaded:
     * false - start ad preloading
     * true - start ad playback
     *
     **/
    private void initScheduler() {
        timer = new Timer();
        if(tHandler == null) {
            tHandler = new Handler();
        }
    }

    /**
     *
     * Need synchronized
     * startScheduler might be call from different threads
     *
     **/
    private synchronized void startScheduler() {
        tTask = new TimerTask() {
            public void run() {
                stopScheduler();
                tHandler.post(new Runnable() {
                    @Override
                    public void run()    {
                        addMessage(String.format("TIME STAMP %s Ad", stateAdLoaded ?"start" :"preload"));
                        if (stateAdLoaded) {
                            startAdman();
                        } else {
                            preloadAdman();
                        }
                        startScheduler();
                    }
                });
            }
        };
        int delay = stateAdLoaded
                ? Math.round((interval*3)/5)
                : Math.round((interval*2)/5);
        Log.d(TAG, String.format("schedule start task after %d milliSec", delay));
        timer.schedule(tTask, delay, delay);
    }

    private void stopScheduler() {
        if (tTask != null){
            Log.d(TAG,"schedule stop (cancel)");
            tTask.cancel();
            tTask = null;
        } else {
            Log.d(TAG,"schedule stop");
        }
    }
    //###########
     //
    private void updateAdRequest() {
        AdmanRequest.Builder builder = this.getAdRequest();
        adman.updateRequest(builder, false);
    };

    private AdmanRequest.Builder getAdRequest() {
        AdmanRequest.Builder builder = (adType.startsWith(AD_TYPE_TRITON) || adType.startsWith(AD_TYPE_TRITON_VIDEO))
                ? getTritonRequest()
                : getAdmanRequest();
        return builder;
    }

    private AdmanRequest.Builder getTritonRequest() {
         final String DEFAULT_HOST       = "cmod.live.streamtheworld.com";
         final String DEFAULT_HOST_756   = "cmod756.live.streamtheworld.com";
         final String DEFAULT_HOST_719   = "cmod719.live.streamtheworld.com";
         final String DEFAULT_STATION_ID = "WEB_SDK_TEST_S1";
         final String DEFAULT_STATION_284343 = "284343";

         String type = this.adType.startsWith(AD_TYPE_TRITON_VIDEO) ? "video" : null;
         Log.d(TAG, "Building an ad request, type: " + type);

         AdRequestBuilder mAdRequestBuilder = new AdRequestBuilder(this);
         // Set the user defined values
         if (type != null && type.startsWith("video")) {
             //video
             mAdRequestBuilder.setHost(DEFAULT_HOST_719);
             mAdRequestBuilder.addQueryParameter("dist", AdRequestBuilder.ASSET_TYPE_VALUE_VIDEO);
             mAdRequestBuilder.addQueryParameter(AdRequestBuilder.STATION_ID, DEFAULT_STATION_284343);
             mAdRequestBuilder.addQueryParameter(AdRequestBuilder.TYPE, AdRequestBuilder.TYPE_VALUE_PREROLL);
         } else {
             //audio
             mAdRequestBuilder.setHost(DEFAULT_HOST_756);
             mAdRequestBuilder.addQueryParameter(AdRequestBuilder.STATION_ID, DEFAULT_STATION_ID);
         }
         return new AdmanRequest.Builder().setAdUrlAPI(mAdRequestBuilder.build());
    }

    private AdmanRequest.Builder getAdmanRequest() {
        /*
       AdmanRequest.Builder requestBuilder = new AdmanRequest.Builder()
                .setSiteId(777)
                .setRegion(Region.EUROPE)
                .setType(Type.VOICE)
                .setPreview(90);
        /**/
        AdmanRequest.Builder requestBuilder = null;
        if (adURLService != null && !adURLService.isEmpty()) {
            requestBuilder = new AdmanRequest.Builder()
                    .setAdUrlAPI(adURLService);
            //"http://cmod756.live.streamtheworld.com/ondemand/ars?dist=Instreamatic&stid=284343&type=preroll&version=1.6.9&fmt=vast&banners=none&tdsdk=android-2.7.2&lsid=gaid%3A2154a670-5cfe-41a0-a0bb-28bf2bce1769&version=1.5.1");
        } else {
            requestBuilder = new AdmanRequest.Builder()
                    .setSiteId(1249)
                    .setRegion(Region.GLOBAL)
                    .setType(Type.VOICE);
        }
        return requestBuilder;
    }

    /**
     *
     * Save the listener object for Service messages
     *
     **/
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    /**
     *
     * Logging Service messages
     *
     **/
    private void addMessage(String msg) {
        Log.d(TAG, msg);
        if(this.listener != null) {
            this.listener.onMessage(String.format("%s %s", TAG, msg));
        }
    }

    //************
    /**
     *
     * Service which is used to request audio focus requestAudioFocus, look at start()
     *
     * In the moment of ad START, there is an event AUDIOFOCUS_LOSS_TRANSIENT and Service will stop audio playback
     * In the moment of ad STOP, there is an event AUDIOFOCUS_GAIN and Service will resume audio playback
     *
     **/

    @Override
    public void onAudioFocusChange(int focusChange) {
        String event = "";
        switch (focusChange) {
            case AudioManager.AUDIOFOCUS_LOSS:
                //Stop the playback
                event = "AUDIOFOCUS_LOSS";
                //stop();
                pause();
                notification.setDemoState(PAUSE);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                //Pause the playback and mark that audio focus was temporary lost
                //shoud be used in order of AUDIOFOCUS_GAIN it will be possible to resume audio playback.
                event = "AUDIOFOCUS_LOSS_TRANSIENT";
                pause();
                notification.setDemoState(PAUSE);
                break;
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                //Change the volume or stop ad playback,
                //this should be checked as well AUDIOFOCUS_LOSS_TRANSIENT.
                event = "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK";
                demoPlayer.setVolume(-0.5f);
                break;
            case AudioManager.AUDIOFOCUS_GAIN:
                event = "AUDIOFOCUS_GAIN";
                if(stateAudioFocus == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK){
                    demoPlayer.setVolume(0.5f);
                }
                else{
                    resume();
                    notification.setDemoState(START);
                }
                break;
        }
        stateAudioFocus = focusChange;
        Log.d(TAG, String.format("onAudioFocusChange: event %s ( %d )", event, focusChange));
    }

    /**
     *
     * Ask for focus
     *
     **/
    private void requestAudioFocus() {
        stateAudioFocus = 0;
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "requestAudioFocus");
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                AudioAttributes mAudioAttributes =
                        new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_MEDIA)
                                .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                                .build();
                AudioFocusRequest mAudioFocusRequest =
                        new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN_TRANSIENT)
                                .setAudioAttributes(mAudioAttributes)
                                .setAcceptsDelayedFocusGain(true)
                                .setOnAudioFocusChangeListener(this)
                                .build();
                audioManager.requestAudioFocus(mAudioFocusRequest);
            } else {
                audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
            }
        }
    }

    /**
     *
     * Free the focus
     *
     **/
    private void abandonAudioFocus() {
        AudioManager audioManager = (AudioManager) this.getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            Log.d(TAG, "abandonAudioFocus");
            audioManager.abandonAudioFocus(this);
        }
    }
    //************
}
